package net.hsbc.demo.util;

import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class VerifyUtil {
	public static boolean checkInput(String number) {
	    if (!StringUtils.isEmpty(number)) {
	        final String regexp = "^([0-9,*]+)$";
	        try {
	            if (Pattern.matches(regexp,number.trim())) {
	            	return true;
	            }
	        } catch (Exception e) {
	        	return false;
	        }
	    }
	    return false;
	}
}

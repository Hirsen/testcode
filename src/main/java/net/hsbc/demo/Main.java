package net.hsbc.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import net.hsbc.demo.model.DataSource;
import net.hsbc.demo.util.VerifyUtil;

public class Main {

	static final int INIT_INDEX=0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		while(true){
			System.out.println("Pless input:");
			String input=scan.nextLine();
			if(!VerifyUtil.checkInput(input)){
				System.out.println("Error input!");
				continue;
			}
			char[] chars = input.toCharArray();
			List<String> outputList = build(chars,INIT_INDEX,new ArrayList<String>());
			for(String output:outputList){
				System.out.println(output);
			}
		}
	}
	public static List<String> build(char[] inputArr,int index,List<String> list){
		for(int i=index;i<inputArr.length;i++){
			String[] valArr = DataSource.data.getByCode(String.valueOf(inputArr[i]));
			if(index==0){
				list=Arrays.asList(valArr);
				build(inputArr,++index,list);
			}else{
					List<String> box=new ArrayList<String>();
					for(String val1:valArr){
						for(String val2:list){
							box.add(val2+val1);
						}
					}
					list=box;
					build(inputArr,++index,list);
			}
		}
		return list;
	}
}

package net.hsbc.demo.model;

public class DataSource {
	public static enum data {
		ARR_MUM_STAR("*", new String[]{"*"}),
		ARR_MUM_ZERO("0", new String[]{""}),
		ARR_MUM_ONE("1", new String[]{""}),
		ARR_MUM_TWO("2", new String[]{"A","B","C"}),
		ARR_MUM_THREE("3", new String[]{"D","E","F"}),
		ARR_MUM_FOUR("4", new String[]{"G","H","I"}),
		ARR_MUM_FIVE("5", new String[]{"J","K","L"}),
		ARR_MUM_SIX("6", new String[]{"M","N","O"}),
		ARR_MUM_SEVN("7", new String[]{"P","Q","R","S"}),
		ARR_MUM_EIGHT("8", new String[]{"T","U","V"}),
		ARR_MUM_NINE("9", new String[]{"W","X","Y","Z"});
	 
	    private String code;
	    private String message[];
	 
	    data(String code, String message[]) {
	        this.code = code;
	        this.message = message;
	    }
	 
	    public static String[] getByCode(String code) {
	        for (data d: data.values()) {
	            if (d.code.equals(code)) {
	                return d.message;
	            }
	        }
	        return null;
	    }
	}
}
